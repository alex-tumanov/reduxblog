import { POSTS } from '../../actions/posts';

const postsObj = {
  100: {
    id: 100,
    title: 'post 100',
    text: '10000000000000',
    comments: [],
  },
  9: {
    id: 9,
    title: 'post 9',
    text: 'lolololo',
    comments: [],
  },
  5: {
    id: 5,
    title: 'post 5',
    text: 'abcdefg',
    comments: [],
  },
};

const commentsObj = {
  0: {
    id: 0,
    text: 'comment 100',
    postId: 100,
    fresh: true,
  },
  1: {
    id: 1,
    text: 'comment 100',
    postId: 100,
    fresh: true,
  },
  2: {
    id: 2,
    text: 'comment 9',
    postId: 9,
    fresh: true,
  },
  10: {
    id: 10,
    text: 'comment 5',
    postId: 5,
    fresh: true,
  }
};

const getRandomNumber = () => (
  Math.round(Math.random() * 1000)
);

const postsInfo = {
  postsObj,
  commentsObj,
};

export const posts = (state = postsInfo, action) => {
  let randomNumber = getRandomNumber();
  switch (action.type) {
    case POSTS.ADD_COMMENT:
      while(state.commentsObj.hasOwnProperty(randomNumber)){
        randomNumber = getRandomNumber();
      }
      let updatedComments = new Object(state.commentsObj);
      updatedComments[randomNumber] = {
        text: action.payload.text,
        postId: action.payload.postId,
        id: randomNumber,
        fresh: true,
      };

      return {
        postsObj: state.postsObj,
        commentsObj: updatedComments,
      };
    default: return state;
  }
};
