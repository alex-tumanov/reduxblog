import { APP } from '../../actions/incrementDecrement.js';

export function counterReducer(state = 0, action) {
  switch (action.type) {
    case APP.INC:
      return state + 1;
    case APP.DEC:
      return state - 1;
    default:
      return state;
  }
}
