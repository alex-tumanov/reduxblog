import React, { Component, PropTypes } from 'react';
import Post from '../../components/Post/index.jsx';

export default class PostsList extends Component {
  static propTypes = {
    posts: PropTypes.array.isRequired,
    addCommentHandler: PropTypes.func.isRequired,
  };
  render() {
    const { posts, addCommentHandler } = this.props;
    const postsList = posts.map((post) => (
      <Post
        post={post}
        key={post.id}
        addComment={(text) => {
          addCommentHandler(text, post.id);
        }}
      />
    ));
    return (
      <div>
        {postsList}
      </div>
    );
  }
}
