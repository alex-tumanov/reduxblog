import React from 'react';
import { Provider } from 'react-redux';
import App from '../App/index.jsx';

export const Root = ({
  store,
}) => (
  <div>
    <Provider store={store}>
      <App />
    </Provider>
  </div>
);
Root.propTypes = {
  store: React.PropTypes.object.isRequired,
};
