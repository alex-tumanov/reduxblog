import React, { Component } from 'react';
import { Router } from 'react-router';
import { history } from '../../index.js';
import routes from '../../routes.js';

export default class App extends Component {
  render() {
    return (
      <Router history={history} routes={routes}/>
    );
  }
}
