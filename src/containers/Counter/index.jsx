import React, { PropTypes, Component } from 'react';
import { Count } from '../../components/Count/index.jsx';
import { Buttons } from '../../components/Buttons/index.jsx';
import { connect } from 'react-redux';
import { dec, inc } from '../../actions/incrementDecrement';

const mapStateToProps = (state) => ({
  count: state,
});

@connect(
  mapStateToProps, {
    decHandler: dec,
    incHandler: inc,
  }
)
export default class Counter extends Component {
  static propTypes = {
    count: PropTypes.object.isRequired,
    incHandler: PropTypes.func.isRequired,
    decHandler: PropTypes.func.isRequired,
  };
  render() {
    const { count, decHandler, incHandler } = this.props;

    return (
      <div>
        <Count count={count.counterReducer} />
        <Buttons
          decHandler={decHandler}
          incHandler={incHandler}
        />
      </div>
    );
  }
}
