import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import PostsList from '../../containers/PostsList/index.jsx';
import { PostHeader } from '../../components/PostHeader/index.jsx';
import { addComment } from '../../actions/posts';

const makeArray = (obj) => (
  Object.keys(obj).map(key => obj[key])
);

const mapStateToProps = (state) => {
  let comments = state.posts.commentsObj;
  let posts = state.posts.postsObj;
  let post;
  let comment;
  
  for(let thisPost in posts) {
    for(let thisComment in comments) {
      post = posts[thisPost];
      comment = comments[thisComment];
      
      if(post.id === comment.postId && comment.fresh) {
        post.comments.push(comment.text);
        comment.fresh = false;
      }
    }
  }

  return {
    posts: makeArray(posts),
  }
};

@connect(
  mapStateToProps, {
    addCommentHandler: addComment,
  }
)
export default class Posts extends Component {
  static propTypes = {
    posts: PropTypes.array.isRequired,
    addCommentHandler: PropTypes.func.isRequired,
  };
  render() {
    const { posts, addCommentHandler } = this.props;
    return (
      <div>
        <PostHeader count={posts.length}/>
        <PostsList
          posts={posts}
          addCommentHandler={addCommentHandler}
        />
      </div>
    );
  }
}
