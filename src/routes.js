import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Main from './components/Main/index.jsx';
import Posts from './containers/Posts/index.jsx';

export default (
  <Route path="/" component={Main}>
    <IndexRoute component={Posts}/>
  </Route>
);
