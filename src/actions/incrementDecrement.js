export const APP = {
  INC: 'INC',
  DEC: 'DEC',
};

export const inc = () =>
  ({
    type: APP.INC,
  });

export const dec = () =>
  ({
    type: APP.DEC,
  });
