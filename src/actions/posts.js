export const POSTS = {
  ADD_COMMENT: 'ADD_COMMENT',
};

export const addComment = (text, postId) => ({
  type: POSTS.ADD_COMMENT,
  payload: { text, postId },
});
