import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/configureStore.js';
import { Root } from './containers/Root/index.jsx';
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux';

const store = configureStore();

export const history = syncHistoryWithStore(browserHistory, store);

render(
  <Root store={store} />,
  document.getElementById('root')
);
