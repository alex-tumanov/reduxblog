import React, { PropTypes } from 'react';

export const PostHeader = ({
  count,
}) => (
  <div>
    <span>Posts count: {count}</span>
    <hr/>
  </div>
);
PostHeader.propTypes = {
  count: PropTypes.number.isRequired,
};
