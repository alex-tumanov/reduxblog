import React, { PropTypes, Component } from 'react';

export default class Post extends Component {
  static propTypes = {
    post: PropTypes.object.isRequired,
    addComment: PropTypes.func.isRequired,
  };
  renderCommentsList = (comments) => (
    comments.map((comment, key) => (
      <li key={key}>{comment}</li>
    ))
  );
  addCommentHandler = (e, addComment) => {
    if (e.key === 'Enter') {
      addComment(this.refs.comment.value);
      this.refs.comment.value = '';
    }
  };
  render() {
    const { post, addComment } = this.props;
    return (
      <div>
        <h1>{post.title}</h1>
        <span>{post.text}</span>
        <h4>Comments: </h4>
        <ul>
          {this.renderCommentsList(post.comments)}
        </ul>
        <div>
          <input
            ref="comment"
            onKeyPress={(e) => {
              this.addCommentHandler(e, addComment);
            }}
            placeholder="Add comment..."
            type="text"
          />
        </div>
        <hr/>
      </div>
    );
  }
}
