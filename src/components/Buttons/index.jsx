import React, { PropTypes } from 'react';

export const Buttons = ({
  decHandler,
  incHandler,
}) => (
  <div>
    <button
      onClick={decHandler}
    >
      -
    </button>
    <button
      onClick={incHandler}
    >
      +
    </button>
  </div>
);
Buttons.propTypes = {
  decHandler: PropTypes.func,
  incHandler: PropTypes.func,
};
