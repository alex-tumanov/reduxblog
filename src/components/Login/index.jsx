import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

@connect(
  null, {
    push,
  }
)
export class Login extends Component {
  static propTypes = {
    push: PropTypes.func.isRequired,
  };
  render() {
    const { push } = this.props;
    return (
      <div>
        <h1>Login page</h1>
        <button
          onClick={() => { push('postlist'); }}
        >
          Login
        </button>
      </div>
    );
  }
}
