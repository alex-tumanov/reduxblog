import React, { PropTypes } from 'react';

export const Count = ({
  count,
}) => (
  <h1>{count}</h1>
);
Count.propTypes = {
  count: PropTypes.number,
};
