import { createStore, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import { rootReducer } from '../reducers/rootReducer/index.js';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

const logger = createLogger();
const routerRedux = routerMiddleware(browserHistory);

export default function configureStore() {
  return createStore(
    rootReducer,
    applyMiddleware(thunk, logger, routerRedux)
  );
}
