/* eslint-disable  */

const path = require('path');
const webpack = require('webpack');

const PATHS = {
  app: path.join(__dirname, 'src/index.js'),
  html: path.join(__dirname, 'src/index.html')
};

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'webpack-hot-middleware/client',
    PATHS.app,
    PATHS.html
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  watch: true,
  watchOptions: {
    aggregateTimeout: 100
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loaders: ['react-hot', 'babel']
      },
      {
        test: /\.html$/,
        loader: 'file?name=[name].[ext]'
      }
    ]
  }
};