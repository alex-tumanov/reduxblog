module.exports = {
    "extends": "airbnb",
    "settings": {
        "import/resolver": {
            "webpack": {
                "config": "webpack.config.dev.js"
            }
        }
    },
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        }
    },
    "rules": {
        //Problem with IDEA autoformat
        "indent": 0,
        //IDEA autoformat
        "react/jsx-space-before-closing": 0,
        //this not convenient if use ES7 decorators
        "react/prefer-stateless-function": 0,
        //Disable for ES7
        "new-cap": 0,
        //Replace for ES7
        "babel/new-cap": 1,
        // its convinient for middlewares, reduce and etc
        "no-param-reassign": [
            "error",
            {
                "props": false
            }
        ]
    },
    "parser": "babel-eslint",
    "plugins": [
        "babel"
    ],
};